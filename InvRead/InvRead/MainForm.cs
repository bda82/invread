﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Media;
using System.Net.Mime;
using System.Threading;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace InvRead
{
	public partial class MainForm : Form
	{
		private CPair Pair1Min;
		private CPair Pair5Min;
		private CPair Pair15Min;
		private CPair Pair30Min;
		private CPair Pair1Hour;
		
		private int selectedIndex = 0;
		private string selectedPair = "";
		
		private const int period1Min = 60;
		private const int period5Min = 300;
		private const int period15Min = 900;
		private const int period30Min = 1800;
		private const int period1Hour = 3600;
		
		private string prevSum1Min;
		private string prevSum5Min;
		private string prevSum15Min;
		private string prevSum30Min;
		private string prevSum1Hour;
		
		public MainForm()
		{
			InitializeComponent();
		}
		void Timer1Tick(object sender, EventArgs e)
		{
			if (selectedPair != "")
			{
				if (!bw1Min.IsBusy) 
				{
					bw1Min.RunWorkerAsync();
				}            
				if (!bw5Min.IsBusy) 
				{
					bw5Min.RunWorkerAsync();
				}
				if (!bw15Min.IsBusy) 
				{
					bw15Min.RunWorkerAsync();
				}	
				if (!bw30Min.IsBusy)
				{
					bw30Min.RunWorkerAsync();
				}
				if (!bw1Hour.IsBusy) 
				{
					bw1Hour.RunWorkerAsync();
				}				
			}		
			
			Thread.Sleep(1000);
			
			timer2.Start();
		}
		void MainFormLoad(object sender, EventArgs e)
		{
			
			var source = new AutoCompleteStringCollection();
            source.AddRange(new string[]{
                "EUR/USD",
                "USD/JPY",
                "GBP/USD",
                "EUR/RUB",
                "USD/RUB",
                "USD/CHF",
                "USD/CAD",
                "EUR/JPY",
                "AUD/USD",
                "NZD/USD",
                "EUR/GBP",
                "EUR/CHF",
                "AUD/JPY",
                "GBP/JPY",
                "CHF/JPY",
                "EUR/CAD",
                "AUD/CAD",
                "CAD/JPY",
                "NZD/JPY",
                "AUD/NZD",
                "GBP/AUD",
                "EUR/AUD",
                "GBP/CHF",
                "EUR/NZD",
                "AUD/CHF",
                "GBP/NZD",
                "USD/SGD",
                "USD/DKK",
                "GBP/CAD"
            });

            cbPair.AutoCompleteCustomSource = source;
            cbPair.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbPair.AutoCompleteSource = AutoCompleteSource.CustomSource;

            Pair1Min = new CPair();
			Pair5Min = new CPair();
			Pair15Min = new CPair();
			Pair30Min = new CPair();
			Pair1Hour = new CPair();
			
			prevSum1Min = "NEUTRAL";
			prevSum5Min = "NEUTRAL";
			prevSum15Min = "NEUTRAL";
			prevSum30Min = "NEUTRAL";
			prevSum1Hour = "NEUTRAL";
			
			notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
			notifyIcon1.BalloonTipText = "Вас приветствует Индикатор investing.com";
            notifyIcon1.ShowBalloonTip(3000);
			
			timer1.Start();
			
		}
		void Bw1MinDoWork(object sender, DoWorkEventArgs e)
		{
			Pair1Min.ReadData(selectedPair, period1Min);
		}
		void Bw1MinProgressChanged(object sender, ProgressChangedEventArgs e)
		{
	
		}
		void Bw1MinRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if ((e.Cancelled))
			{
				MessageBox.Show("1 Min Background Worker Canceled");
			}
			else if (e.Error != null) 
			{
				MessageBox.Show("1 Min Background Worker Error: " + e.Error.Message);
			} 
			else
			{
				tbValue.Text = Pair1Min.Value;
				
				tbChange.Text = Pair1Min.Change + " " + Pair1Min.ChangePre;
				if (Pair1Min.ChangePre.Contains("-"))
				{
					tbChange.BackColor = Color.Coral;
				}
				else if (Pair1Min.ChangePre.Contains("+"))
				{
					tbChange.BackColor = Color.PaleGreen;
				}
				
				switch (Pair1Min.SumStr.ToUpper()) {
					case "STRONG BUY":
						pb1MinBuy.Value = 2;
						pb1MinSell.Value = 0;
						tb1MinSum.Text = "SB";
						break;
					case "BUY":
						pb1MinBuy.Value = 1;
						pb1MinSell.Value = 0;
						tb1MinSum.Text = "B";
						break;
					case "NEUTRAL":
						pb1MinBuy.Value = 0;
						pb1MinSell.Value = 0;
						tb1MinSum.Text = "N";
						break;
					case "SELL":
						pb1MinBuy.Value = 0;
						pb1MinSell.Value = 1;
						tb1MinSum.Text = "S";
						break;
					case "STRONG SELL":
						pb1MinBuy.Value = 0;
						pb1MinSell.Value = 2;
						tb1MinSum.Text = "SS";
						break;
				}
			}	
		}
		void Bw5MinDoWork(object sender, DoWorkEventArgs e)
		{
			Pair5Min.ReadData(selectedPair, period5Min);
		}
		void Bw5MinProgressChanged(object sender, ProgressChangedEventArgs e)
		{
	
		}
		void Bw5MinRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if ((e.Cancelled == true))
			{
				MessageBox.Show("5 Min Background Worker Canceled");
			}
			else if (e.Error != null) 
			{
				MessageBox.Show("5 Min Background Worker Error: " + e.Error.Message);
			} 
			else
			{
				switch (Pair5Min.SumStr.ToUpper()) {
					case "STRONG BUY":
						pb5MinBuy.Value = 2;
						pb5MinSell.Value = 0;
						tb5MinSum.Text = "SB";
						break;
					case "BUY":
						pb5MinBuy.Value = 1;
						pb5MinSell.Value = 0;
						tb5MinSum.Text = "B";
						break;
					case "NEUTRAL":
						pb5MinBuy.Value = 0;
						pb5MinSell.Value = 0;
						tb5MinSum.Text = "N";
						break;
					case "SELL":
						pb5MinBuy.Value = 0;
						pb5MinSell.Value = 1;
						tb5MinSum.Text = "S";
						break;
					case "STRONG SELL":
						pb5MinBuy.Value = 0;
						pb5MinSell.Value = 2;
						tb5MinSum.Text = "SS";
						break;
				}					
			}
			
		}
		void Bw15MinDoWork(object sender, DoWorkEventArgs e)
		{
			Pair15Min.ReadData(selectedPair, period15Min);
		}
		void Bw15MinProgressChanged(object sender, ProgressChangedEventArgs e)
		{
	
		}
		void Bw15MinRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if ((e.Cancelled == true))
			{
				MessageBox.Show("15 Min Background Worker Canceled");
			}
			else if (e.Error != null) 
			{
				MessageBox.Show("15 Min Background Worker Error: " + e.Error.Message);
			} 
			else
			{
				switch (Pair15Min.SumStr.ToUpper()) {
					case "STRONG BUY":
						pb15MinBuy.Value = 2;
						pb15MinSell.Value = 0;
						tb15MinSum.Text = "SB";
						break;
					case "BUY":
						pb15MinBuy.Value = 1;
						pb15MinSell.Value = 0;
						tb15MinSum.Text = "B";
						break;
					case "NEUTRAL":
						pb15MinBuy.Value = 0;
						pb15MinSell.Value = 0;
						tb15MinSum.Text = "N";
						break;
					case "SELL":
						pb15MinBuy.Value = 0;
						pb15MinSell.Value = 1;
						tb15MinSum.Text = "S";
						break;
					case "STRONG SELL":
						pb15MinBuy.Value = 0;
						pb15MinSell.Value = 2;
						tb15MinSum.Text = "SS";
						break;
				}
			}	
		}
		void Bw30MinDoWork(object sender, DoWorkEventArgs e)
		{
			Pair30Min.ReadData(selectedPair, period30Min);
		}
		void Bw30MinProgressChanged(object sender, ProgressChangedEventArgs e)
		{
	
		}
		void Bw30MinRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if ((e.Cancelled == true))
			{
				MessageBox.Show("30 Min Background Worker Canceled");
			}
			else if (e.Error != null) 
			{
				MessageBox.Show("30 Min Background Worker Error: " + e.Error.Message);
			} 
			else
			{
				switch (Pair30Min.SumStr.ToUpper()) {
					case "STRONG BUY":
						pb30MinBuy.Value = 2;
						pb30MinSell.Value = 0;
						tb30MinSum.Text = "SB";
						break;
					case "BUY":
						pb30MinBuy.Value = 1;
						pb30MinSell.Value = 0;
						tb30MinSum.Text = "B";
						break;
					case "NEUTRAL":
						pb30MinBuy.Value = 0;
						pb30MinSell.Value = 0;
						tb30MinSum.Text = "N";
						break;
					case "SELL":
						pb30MinBuy.Value = 0;
						pb30MinSell.Value = 1;
						tb30MinSum.Text = "S";
						break;
					case "STRONG SELL":
						pb30MinBuy.Value = 0;
						pb30MinSell.Value = 2;
						tb30MinSum.Text = "SS";
						break;
				}
			}
		}
		void Bw1HourDoWork(object sender, DoWorkEventArgs e)
		{
			Pair1Hour.ReadData(selectedPair, period1Hour);
		}
		void Bw1HourProgressChanged(object sender, ProgressChangedEventArgs e)
		{
	
		}
		void Bw1HourRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if ((e.Cancelled == true))
			{
				MessageBox.Show("1 Hour Background Worker Canceled");
			}
			else if (e.Error != null) 
			{
				MessageBox.Show("1 Hour Background Worker Error: " + e.Error.Message);
			} 
			else
			{
				switch (Pair1Hour.SumStr.ToUpper()) {
					case "STRONG BUY":
						pb1HourBuy.Value = 2;
						pb1HourSell.Value = 0;
						tb1HourSum.Text = "SB";
						break;
					case "BUY":
						pb1HourBuy.Value = 1;
						pb1HourSell.Value = 0;
						tb1HourSum.Text = "B";
						break;
					case "NEUTRAL":
						pb1HourBuy.Value = 0;
						pb1HourSell.Value = 0;
						tb1HourSum.Text = "N";
						break;
					case "SELL":
						pb1HourBuy.Value = 0;
						pb1HourSell.Value = 1;
						tb1HourSum.Text = "S";
						break;
					case "STRONG SELL":
						pb1HourBuy.Value = 0;
						pb1HourSell.Value = 2;
						tb1HourSum.Text = "SS";
						break;
				}
			}	
		}
		void CbPairSelectedIndexChanged(object sender, EventArgs e)
		{
			selectedIndex = cbPair.SelectedIndex;
			selectedPair = cbPair.Items[selectedIndex].ToString();
		}
		void Timer2Tick(object sender, EventArgs e)
		{
			bool pairIsChanged = false;
			
			string info = "Отслеживаемая пара " + Pair1Min.Name + " изменилась: \n";
			
			if (Pair1Min.SumStr != prevSum1Min)
			{
				info += "(1 Min) " + prevSum1Min + " -> " + Pair1Min.SumStr + "\n";
				pairIsChanged = true;
			}
			if (Pair5Min.SumStr != prevSum5Min)
			{
				info += "(5 Min) " + prevSum5Min + " -> " + Pair5Min.SumStr + "\n";
				pairIsChanged = true;
			}
			if (Pair15Min.SumStr != prevSum15Min)
			{
				info += "(15 Min) " + prevSum15Min + " -> " + Pair15Min.SumStr + "\n";
				pairIsChanged = true;
			}
			if (Pair30Min.SumStr != prevSum30Min)
			{
				info += "(30 Min) " + prevSum30Min + " -> " + Pair30Min.SumStr + "\n";
				pairIsChanged = true;
			}
			if (Pair1Hour.SumStr != prevSum1Hour)
			{
				info += "(1 Hour) " + prevSum1Hour + " -> " + Pair1Hour.SumStr;
				pairIsChanged = true;
			}
			
			if (pairIsChanged)
			{
				notifyIcon1.ShowBalloonTip(3000);
            	notifyIcon1.BalloonTipText = info;

            	PlaySound();
			}
			
			pairIsChanged = false;
			
			prevSum1Min = Pair1Min.SumStr;
			prevSum5Min = Pair5Min.SumStr;
			prevSum15Min = Pair15Min.SumStr;
			prevSum30Min = Pair30Min.SumStr;
			prevSum1Hour = Pair1Hour.SumStr;
		}		
        private void PlaySound()
        {
            var sp = new SoundPlayer(@"bird_2.wav");
            sp.Play();
        }
		void NotifyIcon1MouseDoubleClick(object sender, MouseEventArgs e)
		{
	        if (this.WindowState == FormWindowState.Minimized)
            {
                this.Visible = true;
                this.ShowInTaskbar = true;
                this.WindowState = FormWindowState.Normal;
            }
		}
		void MainFormResize(object sender, EventArgs e)
		{
	        if (this.WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                this.Visible = false;
            }
		}
	}

	public class CPair
	{
		private const int period1Min = 60;
		private const int period5Min = 300;
		private const int period15Min = 900;
		private const int period30Min = 1800;
		private const int period1Hour = 3600;
		
		public string Name;
		public string Value;
		public string Change;
		public string ChangePre;
		public string SumStr;
		public string SumTIStr;
		public string SumMAStr;
		
		public string link;
		public string idValue;
		public string idChange;
		public string idChangePre;
		public string idSumStr;
		public string idSumTIStr;
		public string idSumMAStr;
		
		public void ReadData(string pn, int period)
		{
			Thread.Sleep(500);
			
			const string link1p = @"http://www.investing.com/currencies/";
			string link2p = @"";
			const string link3p = @"-technical?period=";
						
			switch (pn) {
				case "EUR/USD":
					link2p = "eur-usd";
					break;
				case "USD/JPY":
					link2p = "usd-jpy";
					break;
				case "GBP/USD":
					link2p = "gbp-usd";
					break;
				case "EUR/RUB":
					link2p = "eur-rub";
					break;
				case "USD/RUB":
					link2p = "usd-rub";
					break;
				case "USD/CHF":
					link2p = "usd-chf";
					break;
				case "USD/CAD":
					link2p = "usd-cad";
					break;
				case "EUR/JPY":
					link2p = "eur-jpy";
					break;
				case "AUD/USD":
					link2p = "aud-usd";
					break;
				case "NZD/USD":
					link2p = "nzd-usd";
					break;
				case "EUR/GBP":
					link2p = "eur-gbp";
					break;
				case "EUR/CHF":
					link2p = "eur-chf";
					break;
				case "AUD/JPY":
					link2p = "aud-jpy";
					break;
				case "GBP/JPY":
					link2p = "gbp-jpy";
					break;
				case "CHF/JPY":
					link2p = "chf-jpy";
					break;
				case "EUR/CAD":
					link2p = "eur-cad";
					break;
				case "AUD/CAD":
					link2p = "aud-cad";
					break;
				case "CAD/JPY":
					link2p = "cad-jpy";
					break;
				case "NZD/JPY":
					link2p = "nzd-jpy";
					break;
				case "AUD/NZD":
					link2p = "aud-nzd";
					break;
				case "GBP/AUD":
					link2p = "gbp-aud";
					break;
				case "EUR/AUD":
					link2p = "aur-aud";
					break;
				case "GBP/CHF":
					link2p = "gbp-chf";
					break;
				case "EUR/NZD":
					link2p = "eur-nzd";
					break;
				case "AUD/CHF":
					link2p = "aud-chf";
					break;
				case "GBP/NZD":
					link2p = "gbp-nzd";
					break;
				case "USD/SGD":
					link2p = "usd-sgd";
					break;
				case "USD/DKK":
					link2p = "usd-dkk";
					break;
				case "GBP/CAD":
					link2p = "gbp-cad";
					break;
			}			
			
			if (period == period1Min)
			{
				link = link1p + link2p + link3p + "60";
				idValue = @"//*[@id=""last_last""]";
				idChange = @"//*[@id=""quotes_summary_current_data""]/div/div[2]/div[1]/span[2]";
				idChangePre = @"//*[@id=""quotes_summary_current_data""]/div/div[2]/div[1]/span[4]";
				idSumStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[1]/span";
				idSumMAStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[2]/span[2]/span/span";
				idSumTIStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[3]/span[2]/span/span";				
			}
			else if (period == period5Min)
			{
				link = link1p + link2p + link3p + "300";
				idValue = @"//*[@id=""last_last""]";
				idChange = @"//*[@id=""quotes_summary_current_data""]/div/div[2]/div[1]/span[2]";
				idChangePre = @"//*[@id=""quotes_summary_current_data""]/div/div[2]/div[1]/span[4]";
				idSumStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[1]/span";
				idSumMAStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[2]/span[2]/span/span";
				idSumTIStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[3]/span[2]/span/span";				
			}
			else if (period == period15Min)
			{
				link = link1p + link2p + link3p + "900";
				idValue = @"//*[@id=""last_last""]";
				idChange = @"//*[@id=""quotes_summary_current_data""]/div/div[2]/div[1]/span[2]";
				idChangePre = @"//*[@id=""quotes_summary_current_data""]/div/div[2]/div[1]/span[4]";
				idSumStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[1]/span";
				idSumMAStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[2]/span[2]/span/span";
				idSumTIStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[3]/span[2]/span/span";				
			}
			else if (period == period30Min)
			{
				link = link1p + link2p + link3p + "1800";
				idValue = @"//*[@id=""last_last""]";
				idChange = @"//*[@id=""quotes_summary_current_data""]/div/div[2]/div[1]/span[2]";
				idChangePre = @"//*[@id=""quotes_summary_current_data""]/div/div[2]/div[1]/span[4]";
				idSumStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[1]/span";
				idSumMAStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[2]/span[2]/span/span";
				idSumTIStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[3]/span[2]/span/span";				
			}
			else if (period == period1Hour)
			{
				link = link1p + link2p + link3p + "3600";
				idValue = @"//*[@id=""last_last""]";
				idChange = @"//*[@id=""quotes_summary_current_data""]/div/div[2]/div[1]/span[2]";
				idChangePre = @"//*[@id=""quotes_summary_current_data""]/div/div[2]/div[1]/span[4]";
				idSumStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[1]/span";
				idSumMAStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[2]/span[2]/span/span";
				idSumTIStr = @"//*[@id=""techStudiesInnerBoxRightBottom""]/div[3]/span[2]/span/span";				
			}
			
			HtmlAgilityPack.HtmlDocument doc = getDocument(link);
			
			Name = pn;
			Value = getInText(doc, idValue);
			Change = getInText(doc, idChange);
			ChangePre = getInText(doc, idChangePre);
			SumStr = getInText(doc, idSumStr);
		}
		
		private string getInText(HtmlAgilityPack.HtmlDocument doc, string id)
		{
			HtmlAgilityPack.HtmlNode node;
			node = doc.DocumentNode.SelectSingleNode(id);
			return node.InnerText;
		}
		
        private HtmlAgilityPack.HtmlDocument getDocument(string url)
        {
            var webdoc = new HtmlAgilityPack.HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc;
            try
            {
                doc = webdoc.Load(url);
                return doc;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка загрузки страницы: " + ex.Message);
                return null;
            }
        }		
	}
}
