﻿/*
 * Создано в SharpDevelop.
 * Пользователь: user
 * Дата: 16.04.2015
 * Время: 10:49
 * 
 * Для изменения этого шаблона используйте меню "Инструменты | Параметры | Кодирование | Стандартные заголовки".
 */
namespace InvRead
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Timer timer1;
		private System.ComponentModel.BackgroundWorker bw1Min;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.ProgressBar pb1MinBuy;
		private System.Windows.Forms.TextBox tbChange;
		private System.Windows.Forms.TextBox tbValue;
		private System.Windows.Forms.ComboBox cbPair;
		private System.Windows.Forms.TextBox textBox7;
		private System.Windows.Forms.TextBox textBox6;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.ProgressBar pb1HourBuy;
		private System.Windows.Forms.ProgressBar pb30MinBuy;
		private System.Windows.Forms.ProgressBar pb15MinBuy;
		private System.Windows.Forms.ProgressBar pb5MinBuy;
		private System.ComponentModel.BackgroundWorker bw5Min;
		private System.Windows.Forms.ProgressBar pb1HourSell;
		private System.Windows.Forms.ProgressBar pb30MinSell;
		private System.Windows.Forms.ProgressBar pb15MinSell;
		private System.Windows.Forms.ProgressBar pb5MinSell;
		private System.Windows.Forms.ProgressBar pb1MinSell;
		private System.Windows.Forms.TextBox tb1MinSum;
		private System.Windows.Forms.TextBox tb1HourSum;
		private System.Windows.Forms.TextBox tb30MinSum;
		private System.Windows.Forms.TextBox tb15MinSum;
		private System.Windows.Forms.TextBox tb5MinSum;
		private System.ComponentModel.BackgroundWorker bw15Min;
		private System.ComponentModel.BackgroundWorker bw30Min;
		private System.ComponentModel.BackgroundWorker bw1Hour;
		private System.Windows.Forms.Timer timer2;
		private System.Windows.Forms.NotifyIcon notifyIcon1;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.bw1Min = new System.ComponentModel.BackgroundWorker();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tb1HourSum = new System.Windows.Forms.TextBox();
			this.tb30MinSum = new System.Windows.Forms.TextBox();
			this.tb15MinSum = new System.Windows.Forms.TextBox();
			this.tb5MinSum = new System.Windows.Forms.TextBox();
			this.tb1MinSum = new System.Windows.Forms.TextBox();
			this.textBox7 = new System.Windows.Forms.TextBox();
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.pb1HourSell = new System.Windows.Forms.ProgressBar();
			this.pb1HourBuy = new System.Windows.Forms.ProgressBar();
			this.pb30MinSell = new System.Windows.Forms.ProgressBar();
			this.pb30MinBuy = new System.Windows.Forms.ProgressBar();
			this.pb15MinSell = new System.Windows.Forms.ProgressBar();
			this.pb15MinBuy = new System.Windows.Forms.ProgressBar();
			this.pb5MinSell = new System.Windows.Forms.ProgressBar();
			this.pb5MinBuy = new System.Windows.Forms.ProgressBar();
			this.pb1MinSell = new System.Windows.Forms.ProgressBar();
			this.pb1MinBuy = new System.Windows.Forms.ProgressBar();
			this.tbChange = new System.Windows.Forms.TextBox();
			this.tbValue = new System.Windows.Forms.TextBox();
			this.cbPair = new System.Windows.Forms.ComboBox();
			this.bw5Min = new System.ComponentModel.BackgroundWorker();
			this.bw15Min = new System.ComponentModel.BackgroundWorker();
			this.bw30Min = new System.ComponentModel.BackgroundWorker();
			this.bw1Hour = new System.ComponentModel.BackgroundWorker();
			this.timer2 = new System.Windows.Forms.Timer(this.components);
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// timer1
			// 
			this.timer1.Interval = 3000;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// bw1Min
			// 
			this.bw1Min.WorkerReportsProgress = true;
			this.bw1Min.WorkerSupportsCancellation = true;
			this.bw1Min.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Bw1MinDoWork);
			this.bw1Min.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.Bw1MinProgressChanged);
			this.bw1Min.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Bw1MinRunWorkerCompleted);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.tb1HourSum);
			this.groupBox1.Controls.Add(this.tb30MinSum);
			this.groupBox1.Controls.Add(this.tb15MinSum);
			this.groupBox1.Controls.Add(this.tb5MinSum);
			this.groupBox1.Controls.Add(this.tb1MinSum);
			this.groupBox1.Controls.Add(this.textBox7);
			this.groupBox1.Controls.Add(this.textBox6);
			this.groupBox1.Controls.Add(this.textBox5);
			this.groupBox1.Controls.Add(this.textBox4);
			this.groupBox1.Controls.Add(this.textBox3);
			this.groupBox1.Controls.Add(this.pb1HourSell);
			this.groupBox1.Controls.Add(this.pb1HourBuy);
			this.groupBox1.Controls.Add(this.pb30MinSell);
			this.groupBox1.Controls.Add(this.pb30MinBuy);
			this.groupBox1.Controls.Add(this.pb15MinSell);
			this.groupBox1.Controls.Add(this.pb15MinBuy);
			this.groupBox1.Controls.Add(this.pb5MinSell);
			this.groupBox1.Controls.Add(this.pb5MinBuy);
			this.groupBox1.Controls.Add(this.pb1MinSell);
			this.groupBox1.Controls.Add(this.pb1MinBuy);
			this.groupBox1.Controls.Add(this.tbChange);
			this.groupBox1.Controls.Add(this.tbValue);
			this.groupBox1.Controls.Add(this.cbPair);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(224, 207);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			// 
			// tb1HourSum
			// 
			this.tb1HourSum.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tb1HourSum.Location = new System.Drawing.Point(142, 179);
			this.tb1HourSum.Name = "tb1HourSum";
			this.tb1HourSum.Size = new System.Drawing.Size(26, 22);
			this.tb1HourSum.TabIndex = 5;
			// 
			// tb30MinSum
			// 
			this.tb30MinSum.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tb30MinSum.Location = new System.Drawing.Point(142, 153);
			this.tb30MinSum.Name = "tb30MinSum";
			this.tb30MinSum.Size = new System.Drawing.Size(26, 22);
			this.tb30MinSum.TabIndex = 5;
			// 
			// tb15MinSum
			// 
			this.tb15MinSum.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tb15MinSum.Location = new System.Drawing.Point(142, 127);
			this.tb15MinSum.Name = "tb15MinSum";
			this.tb15MinSum.Size = new System.Drawing.Size(26, 22);
			this.tb15MinSum.TabIndex = 5;
			// 
			// tb5MinSum
			// 
			this.tb5MinSum.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tb5MinSum.Location = new System.Drawing.Point(142, 101);
			this.tb5MinSum.Name = "tb5MinSum";
			this.tb5MinSum.Size = new System.Drawing.Size(26, 22);
			this.tb5MinSum.TabIndex = 5;
			// 
			// tb1MinSum
			// 
			this.tb1MinSum.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tb1MinSum.Location = new System.Drawing.Point(142, 75);
			this.tb1MinSum.Name = "tb1MinSum";
			this.tb1MinSum.Size = new System.Drawing.Size(26, 22);
			this.tb1MinSum.TabIndex = 5;
			// 
			// textBox7
			// 
			this.textBox7.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBox7.Location = new System.Drawing.Point(6, 179);
			this.textBox7.Name = "textBox7";
			this.textBox7.ReadOnly = true;
			this.textBox7.Size = new System.Drawing.Size(78, 23);
			this.textBox7.TabIndex = 4;
			this.textBox7.Text = "1  Hour";
			// 
			// textBox6
			// 
			this.textBox6.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBox6.Location = new System.Drawing.Point(6, 153);
			this.textBox6.Name = "textBox6";
			this.textBox6.ReadOnly = true;
			this.textBox6.Size = new System.Drawing.Size(78, 23);
			this.textBox6.TabIndex = 4;
			this.textBox6.Text = "30 Min";
			// 
			// textBox5
			// 
			this.textBox5.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBox5.Location = new System.Drawing.Point(6, 127);
			this.textBox5.Name = "textBox5";
			this.textBox5.ReadOnly = true;
			this.textBox5.Size = new System.Drawing.Size(78, 23);
			this.textBox5.TabIndex = 4;
			this.textBox5.Text = "15 Min";
			// 
			// textBox4
			// 
			this.textBox4.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBox4.Location = new System.Drawing.Point(6, 101);
			this.textBox4.Name = "textBox4";
			this.textBox4.ReadOnly = true;
			this.textBox4.Size = new System.Drawing.Size(78, 23);
			this.textBox4.TabIndex = 4;
			this.textBox4.Text = "5  Min";
			// 
			// textBox3
			// 
			this.textBox3.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBox3.Location = new System.Drawing.Point(6, 75);
			this.textBox3.Name = "textBox3";
			this.textBox3.ReadOnly = true;
			this.textBox3.Size = new System.Drawing.Size(78, 23);
			this.textBox3.TabIndex = 4;
			this.textBox3.Text = "1  Min";
			// 
			// pb1HourSell
			// 
			this.pb1HourSell.ForeColor = System.Drawing.Color.LightSalmon;
			this.pb1HourSell.Location = new System.Drawing.Point(90, 179);
			this.pb1HourSell.Maximum = 2;
			this.pb1HourSell.Name = "pb1HourSell";
			this.pb1HourSell.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.pb1HourSell.Size = new System.Drawing.Size(46, 23);
			this.pb1HourSell.Step = 1;
			this.pb1HourSell.TabIndex = 3;
			// 
			// pb1HourBuy
			// 
			this.pb1HourBuy.ForeColor = System.Drawing.Color.LimeGreen;
			this.pb1HourBuy.Location = new System.Drawing.Point(173, 179);
			this.pb1HourBuy.Maximum = 2;
			this.pb1HourBuy.Name = "pb1HourBuy";
			this.pb1HourBuy.Size = new System.Drawing.Size(45, 23);
			this.pb1HourBuy.Step = 1;
			this.pb1HourBuy.TabIndex = 3;
			// 
			// pb30MinSell
			// 
			this.pb30MinSell.ForeColor = System.Drawing.Color.LightSalmon;
			this.pb30MinSell.Location = new System.Drawing.Point(90, 153);
			this.pb30MinSell.Maximum = 2;
			this.pb30MinSell.Name = "pb30MinSell";
			this.pb30MinSell.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.pb30MinSell.Size = new System.Drawing.Size(46, 23);
			this.pb30MinSell.Step = 1;
			this.pb30MinSell.TabIndex = 3;
			// 
			// pb30MinBuy
			// 
			this.pb30MinBuy.ForeColor = System.Drawing.Color.LimeGreen;
			this.pb30MinBuy.Location = new System.Drawing.Point(173, 153);
			this.pb30MinBuy.Maximum = 2;
			this.pb30MinBuy.Name = "pb30MinBuy";
			this.pb30MinBuy.Size = new System.Drawing.Size(45, 23);
			this.pb30MinBuy.Step = 1;
			this.pb30MinBuy.TabIndex = 3;
			// 
			// pb15MinSell
			// 
			this.pb15MinSell.ForeColor = System.Drawing.Color.LightSalmon;
			this.pb15MinSell.Location = new System.Drawing.Point(90, 127);
			this.pb15MinSell.Maximum = 2;
			this.pb15MinSell.Name = "pb15MinSell";
			this.pb15MinSell.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.pb15MinSell.Size = new System.Drawing.Size(46, 23);
			this.pb15MinSell.Step = 1;
			this.pb15MinSell.TabIndex = 3;
			// 
			// pb15MinBuy
			// 
			this.pb15MinBuy.ForeColor = System.Drawing.Color.LimeGreen;
			this.pb15MinBuy.Location = new System.Drawing.Point(173, 127);
			this.pb15MinBuy.Maximum = 2;
			this.pb15MinBuy.Name = "pb15MinBuy";
			this.pb15MinBuy.Size = new System.Drawing.Size(45, 23);
			this.pb15MinBuy.Step = 1;
			this.pb15MinBuy.TabIndex = 3;
			// 
			// pb5MinSell
			// 
			this.pb5MinSell.ForeColor = System.Drawing.Color.LightSalmon;
			this.pb5MinSell.Location = new System.Drawing.Point(90, 101);
			this.pb5MinSell.Maximum = 2;
			this.pb5MinSell.Name = "pb5MinSell";
			this.pb5MinSell.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.pb5MinSell.Size = new System.Drawing.Size(46, 23);
			this.pb5MinSell.Step = 1;
			this.pb5MinSell.TabIndex = 3;
			// 
			// pb5MinBuy
			// 
			this.pb5MinBuy.ForeColor = System.Drawing.Color.LimeGreen;
			this.pb5MinBuy.Location = new System.Drawing.Point(173, 101);
			this.pb5MinBuy.Maximum = 2;
			this.pb5MinBuy.Name = "pb5MinBuy";
			this.pb5MinBuy.Size = new System.Drawing.Size(45, 23);
			this.pb5MinBuy.Step = 1;
			this.pb5MinBuy.TabIndex = 3;
			// 
			// pb1MinSell
			// 
			this.pb1MinSell.ForeColor = System.Drawing.Color.LightSalmon;
			this.pb1MinSell.Location = new System.Drawing.Point(90, 75);
			this.pb1MinSell.Maximum = 2;
			this.pb1MinSell.Name = "pb1MinSell";
			this.pb1MinSell.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.pb1MinSell.Size = new System.Drawing.Size(46, 23);
			this.pb1MinSell.Step = 1;
			this.pb1MinSell.TabIndex = 3;
			this.pb1MinSell.Tag = "123";
			// 
			// pb1MinBuy
			// 
			this.pb1MinBuy.ForeColor = System.Drawing.Color.LimeGreen;
			this.pb1MinBuy.Location = new System.Drawing.Point(173, 75);
			this.pb1MinBuy.Maximum = 2;
			this.pb1MinBuy.Name = "pb1MinBuy";
			this.pb1MinBuy.Size = new System.Drawing.Size(45, 23);
			this.pb1MinBuy.Step = 1;
			this.pb1MinBuy.TabIndex = 3;
			this.pb1MinBuy.Tag = "123";
			// 
			// tbChange
			// 
			this.tbChange.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tbChange.Location = new System.Drawing.Point(6, 46);
			this.tbChange.Name = "tbChange";
			this.tbChange.ReadOnly = true;
			this.tbChange.Size = new System.Drawing.Size(212, 23);
			this.tbChange.TabIndex = 2;
			// 
			// tbValue
			// 
			this.tbValue.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tbValue.Location = new System.Drawing.Point(90, 19);
			this.tbValue.Name = "tbValue";
			this.tbValue.ReadOnly = true;
			this.tbValue.Size = new System.Drawing.Size(128, 23);
			this.tbValue.TabIndex = 1;
			// 
			// cbPair
			// 
			this.cbPair.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.cbPair.FormattingEnabled = true;
			this.cbPair.Items.AddRange(new object[] {
			"EUR/USD",
			"USD/JPY",
			"GBP/USD",
			"EUR/RUB",
			"USD/RUB",
			"USD/CHF",
			"USD/CAD",
			"EUR/JPY",
			"AUD/USD",
			"NZD/USD",
			"EUR/GBP",
			"EUR/CHF",
			"AUD/JPY",
			"GBP/JPY",
			"CHF/JPY",
			"EUR/CAD",
			"AUD/CAD",
			"CAD/JPY",
			"NZD/JPY",
			"AUD/NZD",
			"GBP/AUD",
			"EUR/AUD",
			"GBP/CHF",
			"EUR/NZD",
			"AUD/CHF",
			"GBP/NZD",
			"USD/SGD",
			"USD/DKK",
			"GBP/CAD"});
			this.cbPair.Location = new System.Drawing.Point(6, 19);
			this.cbPair.Name = "cbPair";
			this.cbPair.Size = new System.Drawing.Size(78, 23);
			this.cbPair.TabIndex = 0;
			this.cbPair.SelectedIndexChanged += new System.EventHandler(this.CbPairSelectedIndexChanged);
			// 
			// bw5Min
			// 
			this.bw5Min.WorkerReportsProgress = true;
			this.bw5Min.WorkerSupportsCancellation = true;
			this.bw5Min.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Bw5MinDoWork);
			this.bw5Min.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.Bw5MinProgressChanged);
			this.bw5Min.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Bw5MinRunWorkerCompleted);
			// 
			// bw15Min
			// 
			this.bw15Min.WorkerReportsProgress = true;
			this.bw15Min.WorkerSupportsCancellation = true;
			this.bw15Min.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Bw15MinDoWork);
			this.bw15Min.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.Bw15MinProgressChanged);
			this.bw15Min.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Bw15MinRunWorkerCompleted);
			// 
			// bw30Min
			// 
			this.bw30Min.WorkerReportsProgress = true;
			this.bw30Min.WorkerSupportsCancellation = true;
			this.bw30Min.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Bw30MinDoWork);
			this.bw30Min.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.Bw30MinProgressChanged);
			this.bw30Min.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Bw30MinRunWorkerCompleted);
			// 
			// bw1Hour
			// 
			this.bw1Hour.WorkerReportsProgress = true;
			this.bw1Hour.WorkerSupportsCancellation = true;
			this.bw1Hour.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Bw1HourDoWork);
			this.bw1Hour.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.Bw1HourProgressChanged);
			this.bw1Hour.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Bw1HourRunWorkerCompleted);
			// 
			// timer2
			// 
			this.timer2.Interval = 3000;
			this.timer2.Tick += new System.EventHandler(this.Timer2Tick);
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
			this.notifyIcon1.Text = "notifyIcon1";
			this.notifyIcon1.Visible = true;
			this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.NotifyIcon1MouseDoubleClick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.ClientSize = new System.Drawing.Size(250, 230);
			this.Controls.Add(this.groupBox1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainForm";
			this.Text = "InvRead";
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.Resize += new System.EventHandler(this.MainFormResize);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

		}
	}
}
